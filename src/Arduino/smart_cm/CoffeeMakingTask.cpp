#include "config.h"
#include "Constants.h"
#include "CoffeeMakingTask.h"
#include "Pot.h"
#include "ButtonImpl.h"
#include "Sonar.h"
#include "MsgService.h"

CoffeeMakingTask::CoffeeMakingTask(Ncapsules* capsules){
  this->capsules=capsules;
  this->coffeeButton=new ButtonImpl(BUTTON_PIN);
  this->sonar=new Sonar(PROX_ECHO_PIN, PROX_TRIG_PIN);
  this->leds=new LedSystem();
  this->pot = new Pot(POT_PIN);
  this->sugar = 0;
}

void CoffeeMakingTask::init(int period){
  Task::init(period);
  state=WAIT;
}

void CoffeeMakingTask::tick(){
	switch (state){
		case WAIT: {
			if (capsules->isReadyForCoffee()){
				state = SUGAR;
			}
			break;
		}
		case SUGAR:{
      int value = pot->getValue()/VALUE;
      if (this->sugar != value){
        this->sugar = value;
        MsgService.sendMsg(String(sugar));
      }
			if (coffeeButton->isPressed()){
        capsules->setCoffeeTaken(false);
				state = MAKING;
				capsules->subQuantity(1);
				MsgService.sendMsg("Making a coffee");
			} else if (!capsules->isReadyForCoffee()){
				state = WAIT;
			}
			break;
		}
		case MAKING:{
			leds->switchOnIn(DT3);
			MsgService.sendMsg("The coffee is ready");
      leds->switchOff();
			state = TAKING;
			this->time = 0;
			break;
		}
		case TAKING:{
    if(time==0){
      time=millis();
    }
			if (sonar->getDistance() < DIST2 || millis() - time > DT4){
        capsules->setCoffeeTaken(true);
				state = WAIT;
        MsgService.sendMsg(" ");
			}
			break;
		}
	}
}
