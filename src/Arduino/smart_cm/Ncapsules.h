#ifndef __NCAPSULES__
#define __NCAPSULES__

class Ncapsules{
  public:

  Ncapsules();//costruttore

  void addQuantity(int value);
  void subQuantity(int value);
  int getQuantity();

  bool isReadyForRefill();
  void setReadyForRefill(bool ready);

  bool isReadyForCoffee();
  void setReadyForCoffee(bool b);

  bool isCoffeeTaken();
  void setCoffeeTaken(bool taken);

private:
  int quantity;
  bool readyForRefill;
  bool readyForCoffee;
  bool coffeeTaken;
};


#endif
