#include "Scheduler.h"
#include "config.h"
#include <avr/sleep.h>
#include <avr/power.h>
#include "Arduino.h"

bool goOn = false;

void Scheduler::init(int basePeriod){
  this->basePeriod = basePeriod;
  timer.setupPeriod(basePeriod);
  nTasks = 0;
}

bool Scheduler::addTask(Task* task){
  if (nTasks < MAX_TASKS-1){
    taskList[nTasks] = task;
    nTasks++;
    return true;
  } else {
    return false;
  }
}

void Scheduler::schedule(){
  if(goOn==false){
    sleepNow();
    detachInterrupt(digitalPinToInterrupt(PIR_PIN));
  }else{
    timer.waitForNextTick();
    for (int i = 0; i < nTasks; i++){
      if (taskList[i]->updateAndCheckTime(basePeriod)){
        taskList[i]->tick();
      }
    }
  }
}

void wakeUpSystem(){
 sleep_disable();
}

void Scheduler::sleepNow(){ 
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  attachInterrupt(digitalPinToInterrupt(PIR_PIN),wakeUpSystem, RISING); 
  sleep_mode();
  goOn = true;
}
