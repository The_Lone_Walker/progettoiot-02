#ifndef __CM_CONTROL_TASK__
#define __CM_CONTROL_TASK__

#include "Task.h"
#include "Ncapsules.h"
#include "PresenceSensor.h"
#include "ProximitySensor.h"

class CMControlTask: public Task {

public:
  CMControlTask(Ncapsules* capsules);
  void init(int period);  
  void tick();

private:
  Ncapsules* capsules;
  PresenceSensor* pir;
  ProximitySensor* sonar;
  int userApproachingTime;

  void sleepNow();

  enum { STANDBY,ON,READY,MAINTENANCE } state;
};

#endif
