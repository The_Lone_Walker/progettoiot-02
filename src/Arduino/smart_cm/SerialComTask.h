#ifndef __SERIAL__
#define __SERIAL__

#include "Task.h"
#include "Ncapsules.h"

class SerialComTask: public Task {

public:
  SerialComTask(Ncapsules* capsules);
  void init(int period);
  void tick();
private:
  Ncapsules* NC;
};

#endif
