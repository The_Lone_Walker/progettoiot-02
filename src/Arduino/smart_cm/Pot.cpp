#include "Pot.h"
#include "Arduino.h"

Pot::Pot(int pin){
	this->pin=pin;
	pinMode(pin,INPUT);
}

int Pot::getValue(){
	int value = analogRead(pin);
  return /*analogRead(pin);*/map(value, 0, 1023, 0, 1023); //togliendo la map (che non servirebbe dava errori)
}
