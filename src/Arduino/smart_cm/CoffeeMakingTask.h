#ifndef __COFFEE__
#define __COFFEE__

#include "Task.h"
#include "Ncapsules.h"
#include "ProximitySensor.h"
#include "Button.h"
#include "LedSystem.h"
#include "Potentiometer.h"

class CoffeeMakingTask: public Task {

public:
  CoffeeMakingTask(Ncapsules* capsules);
  void init(int period);  
  void tick();

private:
  Ncapsules* capsules;
  ProximitySensor* sonar;
  Button* coffeeButton;
  LedSystem* leds;
  Potentiometer* pot;
  int sugar;
  double time;

  enum { WAIT, SUGAR, MAKING, TAKING } state;
};

#endif
