#ifndef __CONSTANTS__
#define __CONSTANTS__

#define DIST1    0.30
#define DIST2    0.10
#define DT1      1000
#define DT2A     5000
#define DT2B     5000
#define DT3      3000
#define DT4      5000
#define NMAX    10  //numero masimo di cpsule da inserire nella macchina quando è in manteinance
#define VALUE 171  //1024 / 6 (sei livelli di zucchero, 1024 valori del pot)

#endif
