#include "CMControlTask.h"
#include "config.h"
#include "Constants.h"
#include "Pir.h"
#include "Sonar.h"
#include "MsgService.h"
#include "Arduino.h"

extern bool goOn;

CMControlTask::CMControlTask (Ncapsules* capsules){
  this->capsules=capsules;
  this->pir=new Pir(PIR_PIN);
  this->sonar=new Sonar(PROX_ECHO_PIN, PROX_TRIG_PIN);
}

void CMControlTask::init(int period){
  Task::init(period);
  state=STANDBY;
}

void CMControlTask::tick(){
	switch (state){
		case STANDBY: {
			//passaggio verso ON
				state=ON;
				userApproachingTime=0;
			break;
		}
		case ON:{
      if(userApproachingTime==0){
        userApproachingTime=millis();
      }
			//passaggio verso READY
			if(sonar->getDistance()<DIST1){
				if((millis()-userApproachingTime)>DT1){ //se il tempo è maggiore di DT1
					MsgService.sendMsg("Welcome!"); //scrivo sulla GUI
					state=READY;
					userApproachingTime=0;
				}
					//passaggio verso STANDBY
			} else if(!pir->isDetected() ){
				if((millis()-userApproachingTime)>DT2B){
					state=STANDBY;
          goOn=false;
				}
			}else{
				userApproachingTime=0;
			}
			break;
		}
		case READY:{
      if(userApproachingTime==0){
        userApproachingTime=millis();
      }
			capsules->setReadyForCoffee(true);
			//passaggio verso ON
			if(sonar->getDistance()>DIST1){
				if((millis()-userApproachingTime)>DT2A && capsules->isCoffeeTaken()){ //se il tempo è maggiore di  DT2A 
					state=ON;
          MsgService.sendMsg(" ");
					userApproachingTime=0;
					capsules->setReadyForCoffee(false);
				}
				//passaggio verso MAINTENANCE
			} else if(capsules->getQuantity() == 0 && capsules->isCoffeeTaken()){
				state=MAINTENANCE;
				MsgService.sendMsg("No more coffee. Waiting for recharge");
        Serial.flush();
				capsules->setReadyForCoffee(false);
			}
			break;
		}
		case MAINTENANCE:{
			capsules->setReadyForRefill(true);
			//passaggio verso STANDBY
			if(capsules->getQuantity()>0){
        goOn=false;
				MsgService.sendMsg("Coffee Refilled " + String(capsules->getQuantity())); //guardare come stampare variabili in c++
        Serial.flush();
				capsules->setReadyForRefill(false);
				state=STANDBY;
			}
			break;
		}
	}
}
