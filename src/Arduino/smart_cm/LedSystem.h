#ifndef __LEDSYSTEM__
#define __LEDSYSTEM__

#include "LedExt.h"


class LedSystem {               //Rappresenta un insieme di tre led che vengono gradualmente accesi in secs tempo.
    
public: 
  LedSystem();
  void switchOnIn(int secs);
  void switchOff();

private:
  LedExt* led1;
  LedExt* led2;
  LedExt* led3;
};

#endif
