#ifndef __POT__
#define __POT__

#include "Potentiometer.h"

class Pot : public Potentiometer {

public :
  Pot(int pin);
  int getValue();

private:
  int pin;
   
};
    
#endif
