#include "LedSystem.h"
#include "Arduino.h"
#include "config.h"
#include "MsgService.h"

#define NLEDS 3

LedSystem::LedSystem(){
  this->led1 = new LedExt(LED1_PIN, 0);
  this->led2 = new LedExt(LED2_PIN, 0);
  this->led3 = new LedExt(LED3_PIN, 0);
}

void LedSystem::switchOnIn(int secs){
  led1->switchOn();
  led2->switchOn();
  led3->switchOn();
  int t = (secs/NLEDS); //secs=3000 NLEDS=3 t=1000
  int steps = t/100;    //steps=10
  float intensity = 256/steps; //intensity= 25,6 (perchè il valore massimo dell'intensità è 256)
 
  for (int i = 0; i < steps; i++){
	  led1->setIntensity((int)(i*intensity));
	  delay(100);
  }
  
  for (int j = 0; j < steps; j++){
	  led2->setIntensity((int)(j*intensity));
	  delay(100);
  }

  for (int k = 0; k < steps; k++){
	  led3->setIntensity((int)(k*intensity));
	  delay(100);
  }
}

void LedSystem::switchOff(){
  led1->switchOff();
  led2->switchOff();
  led3->switchOff();
};
