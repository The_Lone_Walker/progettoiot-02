
/* Authors:
 *  Giovanni Radicchi matr. 789730
 *  Piero Sanchi matr. 793556
 *  Marco Tonetti matr. 789483
*/

#include "MsgService.h"
#include "NCapsules.h"
#include "SerialComTask.h"
#include "CMControlTask.h"
#include "CoffeeMakingTask.h"
#include "Scheduler.h"

Scheduler sched; //inizializzo lo scheduler

void setup() { 
  sched.init(50);
  MsgService.init();
  Ncapsules* capsules = new Ncapsules();

  CoffeeMakingTask* coffeeMaking = new CoffeeMakingTask(capsules);
  coffeeMaking->init(50);
  sched.addTask(coffeeMaking);

  CMControlTask* cmControl = new CMControlTask(capsules);
  cmControl->init(50);
  sched.addTask(cmControl);

  SerialComTask* serialCom = new SerialComTask(capsules);
  serialCom->init(50);
  sched.addTask(serialCom);
}

void loop() {
  sched.schedule();
}
