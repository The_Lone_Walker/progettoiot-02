#include "Ncapsules.h"
#include "Constants.h"

//Ncapsules è la risorsa condivisa che rappresenta il numero di capsule di caffe inserite

Ncapsules::Ncapsules(){
    quantity=NMAX;
  	readyForRefill=false;
   readyForCoffee=false;
   coffeeTaken = true;
}

void Ncapsules::addQuantity(int value){
  if(quantity + value > NMAX){
    quantity = NMAX;
  } else {
    quantity = quantity + value;
  }
}

void Ncapsules::subQuantity(int value){
  if(quantity - value < 0){
    quantity = 0;
  } else {
    quantity = quantity - value;
  }
}

int Ncapsules::getQuantity(){
  return quantity;
}

 bool Ncapsules::isReadyForRefill(){
  return readyForRefill;
}

void Ncapsules::setReadyForRefill(bool value){
    readyForRefill=value;
}

bool Ncapsules::isReadyForCoffee(){
  return readyForCoffee;
}

void Ncapsules::setReadyForCoffee(bool b){
  readyForCoffee=b;
}

bool Ncapsules::isCoffeeTaken(){
  return coffeeTaken;
}

void Ncapsules::setCoffeeTaken(bool taken){
  coffeeTaken = taken;
}
