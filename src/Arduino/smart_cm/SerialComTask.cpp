#include "SerialComTask.h"
#include "MsgService.h"
//#include <stdlib.h>

SerialComTask::SerialComTask(Ncapsules* capsules){
  this->NC=capsules;
}

void SerialComTask::init(int period){
  Task::init(period);
}

void SerialComTask::tick(){
  if (MsgService.isMsgAvailable()) {
    String msg = MsgService.receiveMsg()->getContent();
    int c = msg.toInt();
    if(NC-> isReadyForRefill()){
    NC->addQuantity(c);
    }
  }
}
