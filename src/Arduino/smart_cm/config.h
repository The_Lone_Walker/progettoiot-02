#ifndef __CONFIG__
#define __CONFIG__

#define LED1_PIN 5
#define LED2_PIN 6
#define LED3_PIN 11
#define BUTTON_PIN 3
#define PROX_TRIG_PIN 7
#define PROX_ECHO_PIN 8
#define PIR_PIN 2
#define POT_PIN A0

#endif
