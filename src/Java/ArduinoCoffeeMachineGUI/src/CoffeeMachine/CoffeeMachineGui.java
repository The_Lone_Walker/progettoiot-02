package CoffeeMachine;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import jssc.*;
import msg.SerialCommChannel;
import java.awt.Font;
import javax.swing.JProgressBar;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

public class CoffeeMachineGui {
   
	private JFrame mainMenuFrame = new JFrame("Main Men�");
	private JFrame welcomeFrame = new JFrame("Welcome");
    static SerialCommChannel channel;
    static CoffeeMachineGui window;
    static MonitoringAgent agent;
    
    JPanel panel2 = new JPanel();
    JPanel panel1 = new JPanel();
    JLabel welcomeLabel = new JLabel("WELCOME!");
    JLabel insertLabel = new JLabel("Seleziona porta seriale");
    JLabel lblNewLabel = new JLabel("SMART COFFEE MACHINE");
    JLabel lblNewLabel_1 = new JLabel("Livello di zucchero selezionato: ");
    JProgressBar progressBar = new JProgressBar();
    JLabel lblNumeroDiCapsule = new JLabel("Numero di capsule da inserire:");
    JLabel log = new JLabel();
    JButton btnNewButton = new JButton("INSERISCI CAPSULE");
    JButton nextButton = new JButton("AVANTI");
    JComboBox<Integer> comboBox = new JComboBox<Integer>();
    JComboBox<String> serialComboBox = new JComboBox<String>();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
               window = new CoffeeMachineGui();
               window.welcomeFrame.setVisible(true);
            }
        });
    }

    /**
     * Create the application.
     */
    public CoffeeMachineGui() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
    	
        welcomeFrame.setResizable(false);
        welcomeFrame.setBackground(Color.WHITE);
        welcomeFrame.setBounds(500, 200, 217, 250);
        welcomeFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        welcomeFrame.getContentPane().setLayout(null);
        welcomeFrame.getContentPane().add(panel1);
        
        mainMenuFrame.setResizable(false);
        mainMenuFrame.setBackground(Color.WHITE);
        mainMenuFrame.setBounds(500, 200, 217, 321);
        mainMenuFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainMenuFrame.getContentPane().setLayout(null);
        mainMenuFrame.getContentPane().add(panel2);
        
        panel1.setBackground(new Color(139, 69, 0));
        panel1.setBounds(0, 0, 217,250);
        panel1.setLayout(null);
        panel1.add(serialComboBox);
        panel1.add(welcomeLabel);
        panel1.add(insertLabel);
        panel1.add(nextButton);
		
        panel2.setBackground(new Color(139, 69, 19));
        panel2.setBounds(0, 0, 214, 295);
        panel2.setLayout(null);
        panel2.add(lblNewLabel);
        panel2.add(progressBar);
        panel2.add(lblNewLabel_1);
        panel2.add(lblNumeroDiCapsule);
        panel2.add(comboBox);
        panel2.add(log);
        panel2.add(btnNewButton);

        lblNewLabel.setForeground(new Color(0, 0, 0));
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setBounds(10, 11, 194, 33);
        
        welcomeLabel.setForeground(Color.BLACK);
        welcomeLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
        welcomeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        welcomeLabel.setBounds(10, 11, 194, 33);
      
        insertLabel.setForeground(Color.WHITE);
        insertLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
        insertLabel.setHorizontalAlignment(SwingConstants.CENTER);
        insertLabel.setBounds(10, 50, 194, 33);
       
        progressBar.setMaximum(5);
        progressBar.setMinimum(0);
        progressBar.setValue(0);
        progressBar.setBounds(10, 129, 194, 19);
      
        log.setBounds(10, 55, 194, 33);   
        log.setFont(new Font("Monospaced", Font.BOLD, 15));
        log.setBackground(new Color(139, 69, 19));
        log.setForeground(Color.WHITE);
        
        lblNewLabel_1.setForeground(Color.WHITE);
        lblNewLabel_1.setBounds(10, 99, 194, 19);
        

        lblNumeroDiCapsule.setForeground(Color.WHITE);
        lblNumeroDiCapsule.setBounds(10, 159, 194, 19);
          

        comboBox.setBounds(10, 189, 194, 20);
        for (int i = 0; i < 11; i++) {
			comboBox.addItem(i);
		}      
        
    	serialComboBox.setBounds(10, 100, 194, 20);
		String[] portNames = SerialPortList.getPortNames();
		for (int i = 0; i < portNames.length; i++){
	    	serialComboBox.addItem(portNames[i]);
		}
        
      	nextButton.setBackground(SystemColor.scrollbar);
    	nextButton.setFont(new Font("Tahoma", Font.BOLD, 14));
    	nextButton.setBounds(10, 150, 194, 63);
    	nextButton.addActionListener(new ActionListener() {
    		
    		@Override
			public void actionPerformed(ActionEvent e) {
				try {
					 channel = new SerialCommChannel((String) serialComboBox.getSelectedItem(), 9600);	   
            		 System.out.println("Waiting Arduino for rebooting...");		
            		 Thread.sleep(4000);
            		 System.out.println("Ready.");	
            		 agent = new MonitoringAgent(channel, window);
            		 agent.start();
            		 window.welcomeFrame.setVisible(false);
            		 window.mainMenuFrame.setVisible(true);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}); 
        
        btnNewButton.setBackground(new Color(222, 184, 135));
        btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
        btnNewButton.setBounds(10, 220, 194, 63);
        btnNewButton.addActionListener(new ActionListener() {
      			
			@Override
			public void actionPerformed(ActionEvent e) {
				channel.sendMsg(String.valueOf(comboBox.getSelectedItem()));
			}
		});   
    }   
}
