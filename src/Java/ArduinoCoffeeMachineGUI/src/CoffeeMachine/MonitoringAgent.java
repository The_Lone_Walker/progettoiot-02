package CoffeeMachine;

import msg.*;

public class MonitoringAgent extends Thread {

    SerialCommChannel channel;
    CoffeeMachineGui view;
    String msg;
    Double lv;

    public MonitoringAgent(SerialCommChannel channel, CoffeeMachineGui view ) throws Exception {
        this.channel = channel;
        this.view = view;
    }

    public void run() { 
        while (true) {
         if(channel.isMsgAvailable()) {    	
			try {		
				msg = channel.receiveMsg();	
				lv = Double.parseDouble(msg);		
				Integer lv2 = (int) Math.round(lv);
				view.progressBar.setValue(lv2);
			} catch (Exception e) { 
				view.log.setText(msg);
			}
         }	 
			
        }
    }
}
